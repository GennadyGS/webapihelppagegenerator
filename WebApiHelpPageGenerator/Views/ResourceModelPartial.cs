﻿using WebApiHelpPageGenerator.ModelDescriptions;

namespace WebApiHelpPageGenerator.Views
{
    public partial class ResourceModel
    {
        public ModelDescription Model { get; set; }

        public string HomePageLink { get; set; }
    }
}