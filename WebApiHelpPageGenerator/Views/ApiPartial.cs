﻿using WebApiHelpPageGenerator.Models;

namespace WebApiHelpPageGenerator.Views
{
    public partial class Api
    {
        public HelpPageApiModel Model { get; set; }

        public string HomePageLink { get; set; }
    }
}