﻿using System;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Description;
using System.Linq;
using CommandLine;
using WebApiHelpPageGenerator.ModelDescriptions;
using WebApiHelpPageGenerator.Models;
using WebApiHelpPageGenerator.SampleGeneration;

namespace WebApiHelpPageGenerator
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var options = new CommandLineOptions();
            try
            {
                if (Parser.Default.ParseArguments(args, options))
                {
                    LoadReferences(options);

                    string assemblyPath = options.AssemblyPath;
                    HttpConfiguration config = HttpConfigurationImporter.ImportConfiguration(assemblyPath);
                    HelpPageSampleGenerator sampleGenerator = config.GetHelpPageSampleGenerator();
                    config.EnsureInitialized();
                    Collection<ApiDescription> descriptions = config.Services.GetApiExplorer().ApiDescriptions;
                    IOutputGenerator outputGenerator = LoadOutputGenerator(options);

                    outputGenerator.GenerateIndex(descriptions);

                    foreach (var api in descriptions)
                    {
                        //HelpPageApiModel apiModel = HelpPageConfigurationExtensions.GenerateApiModel(api, config);
                        //if (apiModel != null)
                        //{
                        //    outputGenerator.GenerateApiDetails(apiModel);
                        //}
                        HelpPageApiModel apiModel = config.GetHelpPageApiModel(api);  //HelpPageConfigurationExtensions.GenerateApiModel(api, config);
                        ModelDescriptionGenerator modelDescriptionGenerator = config.GetModelDescriptionGenerator();

                        if (apiModel != null)
                        {
                            outputGenerator.GenerateApiDetails(apiModel);

                            ModelDescription modelDescription;
                            if (modelDescriptionGenerator.GeneratedModels.TryGetValue(apiModel.RequestModelDescription.Name, out modelDescription))
                            {
                                outputGenerator.GenerateResourceDetails(modelDescription);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
            }
        }

        private static IOutputGenerator LoadOutputGenerator(CommandLineOptions options)
        {
            IOutputGenerator outputGenerator = null;
            string extensionAssemblyPath = options.ExtensionAssemblyPath;
            if (!String.IsNullOrEmpty(extensionAssemblyPath))
            {
                var assembly = Assembly.LoadFrom(extensionAssemblyPath);
                foreach (var type in assembly.GetTypes())
                {
                    if ((typeof(IOutputGenerator)).IsAssignableFrom(type))
                    {
                        outputGenerator = (IOutputGenerator)Activator.CreateInstance(type);
                    }
                }
            }
            if (outputGenerator == null)
            {
                outputGenerator = new DefaultOutputGenerator();
            }
            return outputGenerator;
        }

        private static void LoadReferences(CommandLineOptions options)
        {
            foreach (var reference in options.References)
            {
                Assembly.LoadFrom(reference);
            }
        }
    }
}