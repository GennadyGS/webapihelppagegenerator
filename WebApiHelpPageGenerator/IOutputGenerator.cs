﻿using System.Collections.ObjectModel;
using System.Web.Http.Description;
using WebApiHelpPageGenerator.ModelDescriptions;
using WebApiHelpPageGenerator.Models;

namespace WebApiHelpPageGenerator
{
    public interface IOutputGenerator
    {
        void GenerateIndex(Collection<ApiDescription> apis);

        void GenerateApiDetails(HelpPageApiModel apiModel);

        void GenerateResourceDetails(ModelDescription modelDescription);
    }
}